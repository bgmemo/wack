"use strict";

/**
 * ===================
 * Router controller
 * ===================
 */
var main       = require('../app/actions/main'),
shops          = require('../app/actions/shops'),
categories     = require('../app/actions/categories'),
users          = require('../app/actions/users'),
feedbacks      = require('../app/actions/feedbacks'),
path           = require('path'),
jwtConfig      = require('../config/jwt'),
util           = require("util"),
session        = require('express-session'),
MongoStore     = require('connect-mongostore')(session),
passport       = require('passport'),
passportConfig = require('../config/passport'),
flash          = require('connect-flash'),
log4js         = require('log4js'),
logger         = log4js.getLogger();

log4js.replaceConsole();

module.exports = function(express, app, hbs) {
  app.all('*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Token, Content-Type, Authentication');
    res.header('Access-Control-Expose-Headers', '*');
    req.locals = app.locals;
    req.hbs = hbs;

    next();
  });

  //================= GENERAL ROUTER =========================
  var generalRouter = express.Router();

  generalRouter.use(function(req, res, next) {
    app.set('view options', {
      layout: '/layouts/layout'
    });

    next();
  });

  app.use('/', generalRouter);



  //================= API ROUTER =============================
  var apiRouter = express.Router();

  // apiRouter.all('/*', jwtConfig.auth);
  app.use('/api/v1', apiRouter);



  //================= ADMIN ROUTER ===========================
  var adminRouter = express.Router();

  adminRouter.use(session({
    secret: app.locals.secret,
    key: 'sessionID',
    store: new MongoStore({
      'db': 'sessions'
    }),
    resave: true,
    saveUninitialized: true
  }));

  passportConfig(passport);
  adminRouter.use(passport.initialize());
  adminRouter.use(passport.session());
  adminRouter.use(flash());


  adminRouter.use(function(req, res, next) {
    if (!req.isAuthenticated()) {
      if (req.path !== '/login' && req.path !== '/register') {
        res.redirect('/admin/login');
      }
    }
    app.set('view options', {
      layout: '/layouts/admin/layout'
    });

    next();
  });


  adminRouter.post('/register', passport.authenticate('local-signup', {
    successRedirect: '/admin/profile',
    failureRedirect: '/admin/register',
    failureFlash: true
  }));


  adminRouter.post('/login', passport.authenticate('local-login', {
    successRedirect: '/admin/profile',
    failureRedirect: '/admin/login',
    failureFlash: true
  }));

  app.use('/admin', adminRouter);



  //================= GENERAL ==========================

  app.route('/').get(main.index);



  //================= ADMIN ===========================

  app.get('/admin', shops.find);

  app.get('/admin/register', users.registerForm);

  app.get('/admin/login', users.loginForm);

  app.get('/admin/logout', users.logout);

  app.get('/admin/profile', users.profile);

  //---------------------------------------------------
  app.route('/admin/users').get(users.find).post(users.insert);

  app.route('/admin/users/new').get(users.new);

  app.route('/admin/users/:id').get(users.getById).put(users.update).delete(users.delete);

  app.route('/admin/users/:id/edit').get(users.edit);

  //---------------------------------------------------
  app.route('/admin/shops').get(shops.find).post(shops.insert);

  app.route('/admin/shops/near').get(shops.near);

  app.route('/admin/shops/new').get(shops.new);

  app.route('/admin/shops/:id').get(shops.getById).put(shops.update).delete(shops.delete);

  app.route('/admin/shops/:id/edit').get(shops.edit);

  //---------------------------------------------------
  app.route('/admin/tmpshops').get(shops.findTmp).post(shops.insertTmp);

  app.route('/admin/tmpshops/new').get(shops.newTmp);

  app.route('/admin/tmpshops/:id').get(shops.getByIdTmp).put(shops.updateTmp).delete(shops.deleteTmp);

  app.route('/admin/tmpshops/:id/edit').get(shops.editTmp);

  //---------------------------------------------------
  app.route('/admin/categories/:id/shops').get(shops.getByCategoryId);

  app.route('/admin/categories').get(categories.find).post(categories.insert);

  app.route('/admin/categories/new').get(categories.new);

  app.route('/admin/categories/:id').get(categories.getById).put(categories.update).delete(categories.delete);

  app.route('/admin/categories/:id/edit').get(categories.edit);

  //---------------------------------------------------
  app.route('/admin/feedbacks').get(feedbacks.find).post(feedbacks.insert);

  app.route('/admin/feedbacks/new').get(feedbacks.new);

  app.route('/admin/feedbacks/:id').get(feedbacks.getById).put(feedbacks.update).delete(feedbacks.delete);

  app.route('/admin/feedbacks/:id/edit').get(feedbacks.edit);



  //=================  API  ============================


  app.route('/api/v1/users').post(users.fetchToken);

  app.route('/api/v1/users/autoregister').post(users.autoRegister);

  //---------------------------------------------------
  app.route('/api/v1/shops').get(shops.find).post(shops.insert);

  app.route('/api/v1/shops/:id').get(shops.getById).put(shops.update).delete(shops.delete);

  app.route('/api/v1/tmpshops').post(shops.insertTmp);

  //---------------------------------------------------
  app.route('/api/v1/categories/:id/shops').get(shops.getByCategoryId);

  app.route('/api/v1/categories/:id/shops/near').post(shops.near);

  app.route('/api/v1/categories').get(categories.find).post(categories.insert);

  app.route('/api/v1/categories/:id').get(categories.getById).put(categories.update).delete(categories.delete);

  //---------------------------------------------------
  app.route('/api/v1/feedbacks').get(feedbacks.find).post(feedbacks.insert);

  app.route('/api/v1/feedbacks/:id').get(feedbacks.getById).put(feedbacks.update).delete(feedbacks.delete);

  app.route('/api/v1/feedbacks').get(feedbacks.find).post(feedbacks.insert);

  app.route('/api/v1/feedbacks/:id').get(feedbacks.getById).put(feedbacks.update).delete(feedbacks.delete);


  //================= POST ===========================

  // app.route('/api/forum/posts/new').get(posts.new).post(posts.insert);

  // app.route('/api/forum/posts/:id').get(posts.getById);

  // app.route('/api/forum/posts/:postid/comments/new').post(posts.addComment);
}

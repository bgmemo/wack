"use strict";
/**
 *
 *
 *
 */
require(__dirname + '/../models/category');
var mongoose = require('mongoose');
var _ = require('lodash-node');
var CategoryModel = mongoose.model('Category');




exports.index = function(req, res) {
  var category = new CategoryModel;
  res.render('index');
}




exports.new = function(req, res, next) {
  res.render('admin/category/form', {
    category: {}
  });
}




exports.insert = function(req, res, next) {
  var category = new CategoryModel(req.body);

  category.save(function(err, category) {
    if (err) next(err);
    res.redirect('/admin/categories/' + category._id);
  })
}




exports.find = function(req, res, next) {
  //req.query = {"search"="haha"}
  //e.g /api/categorys?search=haha
  if (req.query.search) {
    CategoryModel.find(({
      name: {
        $regex: req.query.search
      }
    }), function(err, categories) {
      if (err) next(err);
      if (categories) {
        res.jsonp(categories);
      } else {
        res.jsonp({});
      }
    })
  } else {
    //e.g /api/v1/categorys?offset=2&limit=2
    CategoryModel.count({}, function(err, total) {

      var offset = (req.query.offset) ? parseInt(req.query.offset) : 0;;
      var limit = (req.query.limit) ? parseInt(req.query.limit) : 10;
      var maxPage = Math.ceil(total / limit);
      var numlst = _.range(1, maxPage + 1);

      var pageNums = new Array();

      for (var i = 0, len = numlst.length; i < len; i++) {
        pageNums.push({
          val: parseInt(i)
        });
      }

      CategoryModel.find({}).sort({
        name: 1
      }).skip(offset * limit).limit(limit).exec(function(err, categories) {
        if (err) next(err);
        switch (req.query.format) {
          case 'jsonp':
            res.jsonp(categories);
            break;
          default:
            res.render('admin/category/categories', {
              limit: limit,
              categories: categories,
              pagenums: pageNums
            })
            break;
        }
      })

    })

  }

}



exports.getById = function(req, res, next) {
  var categoryId = req.params.id;

  CategoryModel.findById(categoryId)
    .exec(function(err, category) {
      if (err) next(err);

      switch (req.query.format) {
        case 'jsonp':
          res.jsonp(category);
          break;
        default:
          req.session.lastpage = '/api/v1/categorys/' + categoryId;
          res.render('admin/category/category', {
            category: category
          });
          break;
      }

    })
}



exports.edit = function(req, res, next) {
  CategoryModel.findById(req.params.id)
    .exec(function(err, category) {
      if (err) next(err);

      res.render('admin/category/form', {
        category: category
      });
    })
}



exports.update = function(req, res, next) {
  CategoryModel.findByIdAndUpdate(req.params.id, req.body, function(err, category) {
    if (err) next(err);

    res.redirect('/api/v1/categories/' + category._id);

  })

}



exports.delete = function(req, res, next) {
  /**
   * DANGEROUS!
   */

  // CategoryModel.remove({
  //   _id: req.params.id
  // }, function(err) {
  //   if (err) next(err);

  //   res.redirect('/api/v1/categories?offset=0');
  // })
}



//
/**
 *
 *
 *
 */

require(__dirname + '/../models/post');

var _ = require('lodash-node'),
  mongoose = require('mongoose'),
  PostModel = mongoose.model('Post');

exports.index = function(req, res, next) {

  // if(req.session.lastpage){
  //   res.redirect(req.session.lastpage);
  // }
  
  PostModel.find({}).exec(function(err, posts) {
    res.render('index', {
      posts: posts
    });
    
  })
  

}




exports.new = function(req, res, next) {

  res.render('newpost');
}




exports.insert = function(req, res, next) {
  var Post = new PostModel();

  Post.title = req.body.title;
  Post.body = req.body.body;

  Post.save(function(err, post) {
    if (err) console.log(err);

    res.redirect('/api/forum/posts/' + post._id);
  })
}



exports.getById = function(req, res, next) {
  var post_id = req.params.id;

  PostModel
    .findById(post_id)
    .exec(function(err, post) {
      if (err) console.log(err);

      res.render('viewpost', {
        post: post
      });

    });
}



exports.addComment = function(req, res, next) {

  PostModel
    .findByIdAndUpdate(req.params.postid, {
      $push: {
        comments: {
          body: req.body.comment_body,
          author: req.body.comment_author_id
        }
      }
    }).exec(function(err, post) {
      if (err) console.log(err);

      res.redirect('/api/forum/posts/' + post._id);
      console.log(post.comments);
    })
}





// exports.edit = function(err, req, res, next) {}


// exports.update = function(err, req, res, next) {}


// exports.delete = function(err, req, res, next) {}


// exports.getAll = function(err, req, res, next) {}




// exports.vote = function(err, req, res, next) {}


// exports.test = function(err, req, res, next) {

//   var regex = req.params.query;

//   console.log("" + regex);

//   ClientModel.find(({
//     name: {
//       $regex: regex
//     }
//   }), function(err, clients) {

//     if (err) console.log(err);

//     res.jsonp(clients);

//   })


// }
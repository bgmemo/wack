/**
 *
 *
 */

require(__dirname + '/../models/user');

var mongoose = require('mongoose');
var _ = require('lodash-node');
var UserModel = mongoose.model('User');
var moment = require('moment');
var jwt = require('jwt-simple');



var generateToken = function(user_Id, secret) {
  var expires = moment().add(6, 'months').valueOf();

  var token = jwt.encode({
    user: user_Id,
    exp: expires
  }, secret);

  return token;
}


exports.autoRegister = function(req, res, next) {
  // POST['device']
  var user = new UserModel(req.body);

  user.save(function(err, user) {
    if (err) next(err);

    var token = generateToken(user._id, req.locals.secret);
    res.jsonp(token);
  })
}


exports.fetchToken = function(req, res, next) {
  var device = req.body.device || {};
  var uuid = device.uuid || {};

  UserModel.findOne({
    'device.uuid': uuid
  }, function(err, user) {
    if (err) next(err);

    if (!user) {
      res.jsonp(404);
    } else {
      var token = generateToken(user._id, req.locals.secret);
      res.jsonp(token);
    }
  });
}




exports.attachReq = function(id, callback) {
  UserModel.findById(id).exec(function(err, user) {
    if (err) callback(err, null);

    callback(null, user);
  })
}




exports.registerForm = function(req, res, next) {
  res.render('admin/register', {
    message: req.flash('signupMessage'),
    route: 'admin.register'
  });

}



exports.loginForm = function(req, res, next) {
  req.hbs.registerHelper('list', function(context, options) {
    var ret = "<ul>";
    for (var i = 0, j = context.length; i < j; i++) {
      ret = ret + options.fn(context[i]);
    }

    return ret + "</ul>";
  });

  res.render('admin/login', {
    message: req.flash('loginMessage'),
    route: 'admin.login'
  });
}




exports.profile = function(req, res, next) {
  res.render('admin/user/profile', {
    user: req.user,
    route: 'admin.user'
  });
}




exports.logout = function(req, res, next) {
  req.logout();
  res.redirect('/admin/login');
}




exports.new = function(req, res, next) {
  res.render('admin/user/form', {
    user: {}
  });
}




exports.insert = function(req, res, next) {
  var user = new UserModel(req.body);

  user.save(function(err, user) {
    if (err) next(err);
    res.redirect('/admin/users/' + user._id);
  })
}




exports.find = function(req, res, next) {
  req.session.lastpage = '/api/v1/users';
  //req.query = {"search"="haha"}
  //e.g /api/users?search=haha
  if (req.query.search) {
    UserModel.find(({
      name: {
        $regex: req.query.search
      }
    }), function(err, users) {
      if (err) next(err);
      if (users) {
        res.jsonp(users);
      } else {
        res.jsonp({});
      }
    })
  } else {
    //e.g /api/v1/users?offset=2&limit=2
    UserModel.count({}, function(err, total) {

      var offset = (req.query.offset) ? parseInt(req.query.offset) : 0;;
      var limit = (req.query.limit) ? parseInt(req.query.limit) : 10;
      var maxPage = Math.ceil(total / limit);
      var numlst = _.range(1, maxPage + 1);

      var pageNums = new Array();

      for (var i = 0, len = numlst.length; i < len; i++) {
        pageNums.push({
          val: parseInt(i)
        });
      }

      UserModel.find({}).sort({
        name: 1
      }).skip(offset * limit).limit(limit).exec(function(err, users) {
        if (err) next(err);
        switch (req.query.format) {
          case 'jsonp':
            res.jsonp(users);
            break;
          default:
            res.render('admin/user/users', {
              limit: limit,
              users: users,
              pagenums: pageNums
            })
            break;
        }
      })
    })
  }
}




exports.getByCategoryId = function(req, res, next) {
  var cagetoryId = req.params.id;

  UserModel.find({
    category: cagetoryId
  }).exec(function(err, users) {
    if (err) next(err);

    switch (req.query.format) {
      case 'jsonp':
        res.jsonp(users);
        break;
      default:
        res.render('admin/user/users', {
          users: users
        });
        break;
    }

  })
}




exports.getById = function(req, res, next) {
  var userId = req.params.id;

  UserModel.findById(userId).exec(function(err, user) {
    if (err) next(err);

    switch (req.query.format) {
      case 'jsonp':
        res.jsonp(user);
        break;
      default:
        req.session.lastpage = '/api/users/' + userId;
        res.render('admin/user/user', {
          user: user
        });
        break;
    }

  })
}




exports.edit = function(req, res, next) {
  UserModel.findById(req.params.id).exec(function(err, user) {
    if (err) next(err);

    res.render('admin/user/form', {
      user: user
    });
  })
}




exports.update = function(req, res, next) {
  UserModel.findByIdAndUpdate(req.params.id, req.body, function(err, user) {
    if (err) next(err);

    res.redirect('/admin/users/' + user._id);
  })
}




exports.delete = function(req, res, next) {
  UserModel.remove({
    _id: req.params.id
  }, function(err) {
    if (err) next(err);

    res.redirect('/admin/users');
  })
}
'use strict';
/**
 *
 *
 *
 */
require('../models/shop');
require('../models/tmpshop');
require('../models/category');

var mongoose = require('mongoose');
var _ = require('lodash-node');
var Q = require('q');
var moment = require('moment');

var errorHandler = require('_/errorhandler');
var pagination = require('_/pagination');
var middleware = require('_/middleware');
var geonear = require('_/geonear');

var log4js = require('log4js');
var logger = log4js.getLogger();
log4js.replaceConsole();

var ShopModel = mongoose.model('Shop');
var TmpShopModel = mongoose.model('TmpShop');
var CategoryModel = mongoose.model('Category');




exports.new = function(req, res, next) {
  var shopCategoryId;
  categoriesPromise().then(function(categories) {
    registerCategoryHelper(req, shopCategoryId);
    res.render('admin/shop/form', {
      shop: {
        geo: {}
      },
      categories: categories
    })
  }).fail(errorHandler);
}




exports.newTmp = function(req, res, next) {
  var shopCategoryId;
  categoriesPromise().then(function(categories) {
    registerCategoryHelper(req, shopCategoryId);
    res.render('admin/shop/tmpform', {
      shop: {
        geo: {}
      },
      categories: categories
    })
  }).fail(errorHandler.log);
}




exports.edit = function(req, res, next) {
  Q.all([
    ShopModel.findById(req.params.id).exec(),
    categoriesPromise()
  ]).spread(function(shop, categories) {
    registerCategoryHelper(req, shop._category);

    res.render('admin/shop/form', {
      shop: shop,
      categories: categories
    })
  }).fail(errorHandler.log);
}




exports.editTmp = function(req, res, next) {
  Q.all([
    TmpShopModel.findById(req.params.id).exec(),
    categoriesPromise()
  ]).spread(function(shop, categories) {
    registerCategoryHelper(req, shop._category);

    res.render('admin/shop/tmpform', {
      shop: shop,
      categories: categories
    })
  }).fail(errorHandler.log);
}




exports.insert = function(req, res, next) {
  var tmp = req.body;
  tmp.creator_ip = req.ip;
  tmp['geo.loc.type'] = 'Point';
  if (tmp['geo.loc.coordinates']) {
    tmp['geo.loc.coordinates'] = _.map(tmp['geo.loc.coordinates'].split(','), function(str) {
      return parseFloat(str);
    })
  }
  var shop = new ShopModel(tmp);

  shop.save(function(err, shop) {
    if (err) next(errorHandler.log(err, 500, 'Shop Controller Error: insert()->shop.save.'));

    var onJsonRes = function() {
      res.status(201).send('Created');
    }
    var onHtmlRes = function() {
      res.redirect('/admin/shops/' + shop._id);
    }
    response(req, onJsonRes, onHtmlRes);
  });
}




exports.insertTmp = function(req, res, next) {
  var tmp = req.body;
  tmp.creator_ip = req.ip;
  tmp['geo.loc.type'] = 'Point';
  if (tmp['geo.loc.coordinates']) {
    tmp['geo.loc.coordinates'] = _.map(tmp['geo.loc.coordinates'].split(','), function(str) {
      return parseFloat(str);
    })
  }
  var shop = new TmpShopModel(tmp);

  shop.save(function(err, shop) {
    if (err) next(errorHandler.log(err, 500, 'Shop Controller Error: insertTmp()->shop.save.'));

    var onJsonRes = function() {
      res.status(201).send('Created');
    }
    var onHtmlRes = function() {
      res.redirect('/admin/tmpshops/' + shop._id);
    }
    response(req, onJsonRes, onHtmlRes);
  });
}




exports.update = function(req, res, next) {
  var shop = req.body;
  shop.creator_ip = req.ip;
  shop['geo.loc.type'] = 'Point';
  if (shop['geo.loc.coordinates']) {
    shop['geo.loc.coordinates'] = _.map(shop['geo.loc.coordinates'].split(','), function(str) {
      return parseFloat(str);
    })
  }

  ShopModel.findByIdAndUpdate(req.params.id, shop, function(err, shop) {
    if (err) next(errorHandler.log(err, 500, 'Shop Controller Error: ShopModel.findByIdAndUpdate.'));

    var onJsonRes = function() {
      res.jsonp(200);
    }
    var onHtmlRes = function() {
      res.redirect('/admin/shops/' + shop._id);
    }
    response(req, onJsonRes, onHtmlRes);
  });
}




exports.updateTmp = function(req, res, next) {
  var tmpshop = req.body;
  tmpshop['geo.loc.type'] = 'Point';
  if (tmpshop['geo.loc.coordinates']) {
    tmpshop['geo.loc.coordinates'] = _.map(tmpshop['geo.loc.coordinates'].split(','), function(str) {
      return parseFloat(str);
    })
  }
  var newshop = new ShopModel(tmpshop);

  newshop.save(function(err, shop) {
    if (err) next(errorHandler.log(err, 500, 'Shop Controller Error: updateTmp()->newshop.save.'));

    //Approves it, and links with the new shop ID
    TmpShopModel.findByIdAndUpdate(req.params.id, {
      approved: true,
      _shop: shop._id
    }, function(err, shop) {
      if (err) next(errorHandler.log(err, 500, 'Shop Controller Error: updateTmp()->TmpShopModel.findByIdAndUpdate.'));

      var onJsonRes = function() {
        res.jsonp(200);
      }
      var onHtmlRes = function() {
        res.redirect('/admin/tmpshops/');
      }
      response(req, onJsonRes, onHtmlRes);
    })
  })
}




exports.find = function(req, res, next) {
  // logger.debug(req.headers);
  if (req.query.search) {
    search(req, res, next);
  } else {
    paginate(req, res, next);
  }
}




/**
 *
 * /api/v1/shops?offset=2&limit=2
 *
 * @see  find()
 */
function paginate(req, res, next) {
  var offset = (req.query.offset) ? parseInt(req.query.offset) : 0;
  var limit = (req.query.limit) ? parseInt(req.query.limit) : 10;
  var sort = {
    name: 1
  };
  var pupulate = '_category';

  pagination.results(ShopModel, offset, limit, sort, pupulate)
    .then(function(results) {
      var onJsonRes = function() {
        res.jsonp(results.partial);
      }

      var onHtmlRes = function() {
        res.render('admin/shop/shops', {
          limit: limit,
          shops: results.partial,
          pagenums: results.pageNumList,
          route: 'admin.shops'
        })
      }

      response(req, onJsonRes, onHtmlRes);
    })
}




exports.findTmp = function(req, res, next) {
  var offset = (req.query.offset) ? parseInt(req.query.offset) : 0;
  var limit = (req.query.limit) ? parseInt(req.query.limit) : 10;
  var sort = {
    name: 1
  };
  var pupulate = '_category';

  pagination.results(TmpShopModel, offset, limit, sort, pupulate)
    .then(function(results) {

      var shops = _.map(results.partial, function(shop) {
        var newShop = shop;
        newShop.created_at = moment(shop.created_at).fromNow(); //calender()
        return newShop;
      });

      var onJsonRes = function() {
        res.jsonp(shops);
      }

      var onHtmlRes = function() {
        res.render('admin/shop/tmpshops', {
          limit: limit,
          shops: shops,
          pagenums: results.pageNumList,
          route: 'admin.tmpshops'
        })
      }

      response(req, onJsonRes, onHtmlRes);
    })
}




/**
 * req.body :
 *
 * @lng (body)
 * @lat (body)
 * @radius (body)
 * @id (params)
 */
exports.near = function(req, res, next) {

  // opts = req.body
  var opts = {
    lng: req.body.lng,
    lat: req.body.lat,
    radius: req.body.radius,
    _category: req.body.categoryid
  }

  geonear.nearestShops(opts).then(function(shops) {
    res.jsonp(shops);
  })
}



/**
 * /categories/:id/shops?offset=0&limit=10
 */
exports.getByCategoryId = function(req, res, next) {
  var cagetoryId = req.params.id;

  var offset = (req.query.offset) ? parseInt(req.query.offset) : 0;
  var limit = (req.query.limit) ? parseInt(req.query.limit) : 15;
  var sort = {
    name: 1
  };
  var pupulate = '_category';
  var findQuery = {
    _category: cagetoryId
  };

  pagination.results(ShopModel, offset, limit, sort, pupulate, findQuery)
    .then(function(results) {

      var onJsonRes = function() {
        res.jsonp(results.partial);
      }

      var onHtmlRes = function() {
        res.render('admin/shop/shops', {
          limit: limit,
          shops: results.partial,
          pagenums: results.pageNumList,
          route: 'admin.shops'
        })
      }

      response(req, onJsonRes, onHtmlRes);
    }, function(err) {
      next(errorHandler.log(err, 500, 'Shop Controller Error: getByCategoryId().'));
    })
}





exports.getById = function(req, res, next) {
  var shopId = req.params.id;

  ShopModel.findById(shopId).exec(function(err, shop) {
    if (err) next(errorHandler.log(err, 500, 'Shop Controller Error: getById().'));

    var onJsonRes = function() {
      res.jsonp(shop);
    }
    var onHtmlRes = function() {
      res.render('admin/shop/shop', {
        shop: shop
      });
    }
    response(req, onJsonRes, onHtmlRes);
  })
}





exports.getByIdTmp = function(req, res, next) {
  var shopId = req.params.id;

  TmpShopModel.findById(shopId).exec(function(err, shop) {
    if (err) next(errorHandler.log(err, 500, 'Shop Controller Error: getByIdTmp().'));

    var onJsonRes = function() {
      res.jsonp(shop);
    }
    var onHtmlRes = function() {
      res.render('admin/shop/tmpshop', {
        shop: shop
      });
    }
    response(req, onJsonRes, onHtmlRes);
  })
}





exports.delete = function(req, res, next) {
  ShopModel.remove({
    _id: req.params.id
  }, function(err) {
    if (err) next(errorHandler.log(err, 500, 'Shop Controller Error: delete().'));

    res.redirect('/admin/shops');
  })
}





exports.deleteTmp = function(req, res, next) {
  TmpShopModel.remove({
    _id: req.params.id
  }, function(err) {
    if (err) next(errorHandler.log(err, 500, 'Shop Controller Error: deleteTmp().'));

    res.redirect('/admin/tmpshops');
  })
}





function response(req, responseInJson, responseInHtml) {
  switch (req.query.format) {
    case 'jsonp':
      responseInJson();
      break;

    default:
      responseInHtml();
      break;
  }
}




function registerCategoryHelper(req, id) {
  req.hbs.registerHelper('category', function(context, options) {
    var ret = "";
    for (var i = 0, j = context.length; i < j; i++) {
      if (context[i]._id.equals(id)) {
        ret += '<option value="' + context[i]._id + '" selected>' + context[i].name + '</option>';
      } else {
        ret += '<option value="' + context[i]._id + '" >' + context[i].name + '</option>';
      }
    }
    return ret;
  })
}




function categoriesPromise() {
  var deferred = Q.defer();
  CategoryModel.find({}).exec().then(function(categories) {
    deferred.resolve(categories);
  }, function(err) {
    deferred.reject(err);
  })
  return deferred.promise;
}




/**
 *
 * /api/v1/shops?search=haha
 *
 * req.query = {"search"="haha"}
 *
 * @see  find()
 */
function search(req, res, next) {
  ShopModel.find(({
    name: {
      $regex: req.query.search
    }
  }), function(err, shops) {
    if (err) next(errorHandler.log(err));

    if (shops) {
      res.jsonp(shops);
    } else {
      res.jsonp({});
    }
  })
}





//
/**
 *
 *
 *
 */
require(__dirname + '/../models/feedback');
var mongoose = require('mongoose');
var _ = require('lodash-node');
var moment = require('moment');
var FeedbackModel = mongoose.model('Feedback');




exports.index = function(req, res) {

  var feedback = new FeedbackModel;
  res.render('admin/index');
}




exports.new = function(req, res, next) {

  res.render('admin/feedback/form', {
    feedback: {}
  });
}




exports.insert = function(req, res, next) {

  var feedback = new FeedbackModel(req.body);

  feedback.save(function(err, feedback) {

    if (err) next(err)

    if (req.query.format === 'jsonp') {
      res.status(201).send('Created');
    }
  })
}




exports.find = function(req, res, next) {

  //req.query = {"search"="haha"}
  //e.g /api/feedbacks?search=haha
  if (req.query.search) {
    FeedbackModel.find(({
      name: {
        $regex: req.query.search
      }
    }), function(err, feedbacks) {
      if (err) next(err);
      if (feedbacks) {
        res.jsonp(feedbacks);
      } else {
        res.jsonp({});
      }
    })
  } else {
    //e.g /api/v1/feedbacks?offset=2&limit=2
    FeedbackModel.count({}, function(err, total) {

      var offset = (req.query.offset) ? parseInt(req.query.offset) : 0;;
      var limit = (req.query.limit) ? parseInt(req.query.limit) : 10;
      var maxPage = Math.ceil(total / limit);
      var numlst = _.range(1, maxPage + 1);

      var pageNums = new Array();

      for (var i = 0, len = numlst.length; i < len; i++) {
        pageNums.push({
          val: parseInt(i)
        });
      }

      FeedbackModel.find({}).sort({
        name: 1
      }).skip(offset * limit).limit(limit).lean().exec(function(err, feedbacks) {

        if (err) next(err);


        var feedbacks = _.map(feedbacks, function(feedback) {

          var newFeedback = feedback;
          newFeedback.created_at = moment(feedback.created_at).fromNow();

          return newFeedback;
        });


        switch (req.query.format) {

          case 'jsonp':
            res.jsonp(feedbacks);
            break;

          default:
            res.render('admin/feedback/feedbacks', {
              limit: limit,
              feedbacks: feedbacks,
              pagenums: pageNums
            })
            break;
        }
      })

    })

  }

}




exports.getById = function(req, res, next) {

  var feedbackId = req.params.id;

  FeedbackModel.findById(feedbackId).exec(function(err, feedback) {

    if (err) next(err);

    FeedbackModel.findByIdAndUpdate(feedback._id, {
      read: true
    }, function(err, feedback) {

      if (err) next(err);

      switch (req.query.format) {
        case 'jsonp':
          res.jsonp(feedback);
          break;

        default:
          req.session.lastpage = '/api/feedbacks/' + feedbackId;
          res.render('admin/feedback/feedback', {
            feedback: feedback
          });
          break;
      }

    })

  })
}





exports.edit = function(req, res, next) {

  FeedbackModel.findById(req.params.id)
    .exec(function(err, feedback) {
      if (err) next(err);

      res.render('admin/feedback/form', {
        feedback: feedback
      });
    })
}




exports.update = function(req, res, next) {

  FeedbackModel.findByIdAndUpdate(req.params.id, req.body, function(err, feedback) {
    if (err) next(err);

    res.redirect('/api/v1/feedbacks/' + feedback._id);

  })

}



exports.delete = function(req, res, next) {

  FeedbackModel.remove({
    _id: req.params.id
  }, function(err) {
    if (err) next(err);

    res.redirect('/admin/feedbacks/');
  })
}



//
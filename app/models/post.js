/**
 *
 * Mongoose Model -- Post
 * 
 * 
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PostSchema = new Schema({
  title: {
    type: String,
    trim: true
  },

  author: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },

  body: {
    type: String,
    trim: true
  },

  created_at: {
    type: Date,
    default: Date.now
  },

  updated_at: {
    type: Date
  },

  tags: [{
    type: String,
    trim: true
  }],

  vote: {
    up: Number,
    down: Number
  },

  comments: [{
    body: String,
    author: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    created_at: {
      type: Date,
      default: Date.now
    },
    vote: {
      up: Number,
      down: Number
    }
  }]

});


mongoose.model('Post', PostSchema);

module.exports = mongoose.model('Post');
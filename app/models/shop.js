/**
 * ===================
 * Model of Shop
 * ===================
 *
 *
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CREATEDBY = 'SYSTEM ADMIN OWNER USER'.split(' ');

var ShopSchema = new Schema({
  name: {
    type: String,
    trim: true
  },
  address: {
    type: String,
    trim: true
  },
  geo: {
    street: {
      type: String
    },
    route: {
      type: String
    },
    district: {
      type: String
    },
    city: {
      type: String
    },
    zipcode: {
      type: String
    },
    loc: {
      'type': {
        type: String
      },
      'coordinates': []
    }
  },
  phone: {
    type: String,
    trim: true
  },
  mobile: {
    type: String,
    trim: true
  },
  email: {
    type: String,
    trim: true
  },
  website: {
    type: String,
    trim: true
  },
  desc: {
    type: String,
    trim: true
  },
  _category: {
    type: Schema.Types.ObjectId,
    ref: 'Category'
  },
  _owner: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  _tmpshop: {
    type: Schema.Types.ObjectId,
    ref: 'TmpShop'
  },
  created_by: {
    type: String,
    default: 'SYSTEM',
    enum: CREATEDBY
  },
  created_at: {
    type: Date,
    default: Date.now
  },
  creator_ip: {
    type: String,
    trim: true
  }
});

// ShopSchema.index({
//   'geo.loc': '2dsphere'
// });

// ShopSchema.index({
//   "geo.loc": "geoHaystack",
//   type: 1
// }, {
//   bucketSize: 1
// });

mongoose.model('Shop', ShopSchema);

module.exports = mongoose.model('Shop');
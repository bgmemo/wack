/**
 * ======================
 * Model of User feedback
 * ======================
 *
 *
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FeedbackSchema = new Schema({
  email: {
    type: String,
    trim: true
  },
  content: {
    type: String,
    trim: true
  },
  _author: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  created_at: {
    type: Date,
    default: Date.now
  },
  read: {
    type: Boolean,
    default: false
  }
});

mongoose.model('Feedback', FeedbackSchema);

module.exports = mongoose.model('Feedback');
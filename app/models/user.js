/**
 *
 */

var mongoose = require('mongoose'),
  autoIncrement = require('mongoose-auto-increment'),
  bcrypt = require('bcrypt-nodejs'),
  Schema = mongoose.Schema;

var USERTYPE = 'ADMIN USER CLIENT TEMP'.split(' ');
var LOCATION = 'AUCKLAND WELLINGTON CHRISTCHURCH HAMILTON DUNETIN QUEENSTON'.split(' ');

var UserSchema = new Schema({
  uid: {
    type: String
  },
  local: {
    username: String,
    password: String,
  },
  device: {
    type: Schema.Types.Mixed
  },
  location: {
    type: String,
    default: 'AUCKLAND',
    enum: LOCATION
  },
  role: {
    type: String,
    default: 'USER',
    enum: USERTYPE
  },
  joined_at: {
    type: Date,
    default: Date.now
  },
  last_active: {
    type: Date,
    default: Date.now
  }
});


// generating a hash
UserSchema.methods.generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
UserSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.local.password);
};

autoIncrement.initialize(mongoose.connection);

UserSchema.plugin(autoIncrement.plugin, {
    model: 'User',
    field: 'uid',
    startAt: 100000,
    incrementBy: 1
});

mongoose.model('User', UserSchema);

module.exports = mongoose.model('User');
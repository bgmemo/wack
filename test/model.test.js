var env = process.env.NODE_ENV || 'test';
console.log('NODE_ENV: ' + process.env.NODE_ENV);
if (process.env.NODE_ENV !== 'test') {
  console.log("Woops, you want NODE_ENV=test before you try this again!");
  process.exit(1);
}

require('dotenv').load();

var routes = require('../app/routes');
var passport = require('passport');
var express = require('express');
var hbs = require('hbs');
var app = express();
var mongoose = require('mongoose');
var config = require('../config/config');
var request = require('supertest');
var should = require('should');

var Shop = require('../app/models/shop');
var Category = require('../app/models/category');

routes(express, app, passport, hbs);

/////////////////////// TEST ENVIRONEMNT CONFIG //////////////////////////////

beforeEach(function(done) {

  function clearDB() {
    for (var i in mongoose.connection.collections) {
      mongoose.connection.collections[i].remove(function(err) {
        if (err) {
          throw err;
        }
      });
    }
    return done();
  }

  function reconnect() {
    mongoose.connect(config.mongo_url(env), function(err) {
      if (err) {
        throw err;
      }
      return clearDB();
    });
  }

  function checkState() {
    switch (mongoose.connection.readyState) {
      case 0:
        reconnect();
        break;
      case 1:
        clearDB();
        break;
      default:
        process.nextTick(checkState);
    }
  }

  checkState();
});


afterEach(function(done) {
  mongoose.disconnect();
  return done();
});






/////////////////////// TEST BEGINS ///////////////////////////////////


describe('Model of Shop & Category', function() {

  beforeEach(function(done) {

    Shop.remove();
    Category.remove();

    done();
  });

  after(function(done) {
    Shop.remove(function(err) {
      if (err) done(err);
    });
    Category.remove(function(err) {
      if (err) done(err);
    });
    done();
  });



  describe('#save()', function() {
    var shop, category;


    var mockShop = {
      "name": "test shop name",
      "phone": "09-123-4567",
      "_category": mongoose.Types.ObjectId("540447e6b03ba3f46bd9d198"),
      "address": "473 Khyber Pass Rd, Newmarket, Auckland",
      "created_at": new Date(1409566697342),
      "created_by": "SYSTEM",
      "geo": {
        "street": "473",
        "route": "Khyber Pass Rd",
        "district": "Newmarket",
        "city": "Auckland",
        "loc": {
          "type": "Point",
          "coordinates": [
            174.77686650000001123, -36.867521500000002277
          ]
        }
      }
    };

    var mockCategory = {
      "_id": mongoose.Types.ObjectId("540447e6b03ba3f46bd9d198"),
      "name": "餐馆",
      "_ancestors": []
    }


    beforeEach(function(done) {

      mongoose.models = {};
      mongoose.modelSchemas = {};

      category = new Category(mockCategory);
      category.save(function(err, category) {
        if (err) done(err);

        mockShop._category = category._id;
        shop = new Shop(mockShop);

        done();
      });
    });

    it('should have required properties', function(done) {
      shop.save(function(err, shop) {
        if (err) done(err);

        should.not.exist(err);
        shop.should.have.property('name', 'test shop name');
        shop.should.have.property('_category', mongoose.Types.ObjectId("540447e6b03ba3f46bd9d198"));

        done();
      });
    });


  })






})
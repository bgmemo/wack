'use strict';
/**
 *
 */
var Q = require('q');
var _ = require('lodash-node');

var pagination = function pagination() {
  var total;
  var self = this;


  this.results = function(Model, offset, limit, sort, pupulate, findQuery) {
    var deffered = Q.defer();
    self.numOf(Model).then(function(total) {

      self.getPartial(Model, offset, limit, sort, pupulate, findQuery).then(function(partial) {

        var res = {
          pageNumList: self.pageNumList(total, limit),
          partial: partial
        };

        deffered.resolve(res);

      })
    })
    return deffered.promise;
  }



  this.numOf = function(Model) {
    var deffered = Q.defer();

    Model.count({}, function(err, num) {
      deffered.resolve(num);
    })
    return deffered.promise;
  }



  this.getPartial = function(Model, offset, limit, sort, pupulate, findQuery) {
    var deffered = Q.defer();

    Model.find(findQuery)
      .sort(sort)
      .skip(offset * limit)
      .limit(limit)
      .populate(pupulate)
      .exec(function(err, results) {

        if (err) next(err);
        deffered.resolve(results);
      })
    return deffered.promise;
  }



  this.pageNumList = function(total, limit) {
    var pageList = new Array();
    var maxPage = Math.ceil(total / limit);
    var numlst = _.range(1, maxPage + 1);

    for (var i = 0, len = numlst.length; i < len; i++) {
      pageList.push({
        val: parseInt(i)
      })
    }
    return pageList;
  }


}

module.exports = new pagination();
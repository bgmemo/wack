'use strict';
/**
 *
 */
var log4js = require('log4js');
var logger = log4js.getLogger();
log4js.replaceConsole();

exports.log = function(err, status, message) {
  var msg = message || 'Shop Controller Error.'
  var error = new Error(msg);
  error.status = status || 500;
  error.inner = err;
  logger.error(error);
  return error;
}

exports = module.exports;
'use restrict';
/**
 *
 */
require('_/errorhandler');

var mongoose = require('mongoose');
var ShopModel = mongoose.model('Shop');
var Q = require('q');
var _ = require('lodash-node');

var log4js = require('log4js');
var logger = log4js.getLogger();
log4js.replaceConsole();

const DIS_FIELD = 'dis';
const EARTH_RADIUS = 6371000; //meters


/**
 * @return <Promise> Shops
 */
exports.nearestShops = function(opts) {

  var deferred = Q.defer();
  var settings = exports.settings(opts);
  var geoLoc = exports.getCoords(settings.lng, settings.lat);
  var query = {
    _category: mongoose.Types.ObjectId(settings._category)
  };

  ShopModel.aggregate().near({
    near: geoLoc,
    distanceField: DIS_FIELD,
    maxDistance: settings.radius / EARTH_RADIUS,
    distanceMultiplier: EARTH_RADIUS,
    query: query,
    num: settings.limit,
    spherical: true,
    lean: true
  }).exec(function(err, shops) {
    if (err) {
      logger.error(err);
      deferred.reject(err);
    }

    var results = new Array();

    _(shops).forEach(function(shop) {
      shop.dis = Math.round(shop.dis);
      results.push(shop);
    });

    deferred.resolve(results);
  });

  return deferred.promise;
}




exports.getCoords = function(lng, lat) {
  var coords = new Array();
  coords.push(parseFloat(lng));
  coords.push(parseFloat(lat));
  return coords;
}



exports.settings = function(opts) {
  var defaults = {
    lng: 174.7633315,
    lat: -36.8484597,
    radius: 10000,
    limit: 20,
    _category: '540447e6b03ba3f46bd9d198'
  }

  var settings = _.extend(defaults, opts, function(a, b) {
    if (b) {
      return b;
    } else {
      return a;
    }
  });

  return settings;
}


module.exports = exports;
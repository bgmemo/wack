'use strict';


/**
 *
 * jQuery invoked
 *
 */
$(function() {

  // Generate geo info in shop.edit page
  $('#btn-geo').click(function() {
    var address = $("input[name='address']").val();
    ajaxGeo(address);
  })


  $('#select-geo').on('change', function() {

    var loc = $(this).val().split('|');

    var street = loc[0];
    var route = loc[1];
    var district = loc[2];
    var city = loc[3];
    var zipcode = loc[4];

    var cord =[];
    cord.push(parseFloat(loc[6]));
    cord.push(parseFloat(loc[5]));
    
    $("input[name='geo.route']").val(street + ' ' + route);
    $("input[name='geo.district']").val(district);
    $("input[name='geo.city']").val(city);
    $("input[name='geo.zipcode']").val(zipcode);
    $("input[name='geo.loc.coordinates']").val(cord);

    $("input[name='address']").val(street + ' ' + route + ', ' + district + ', ' + city);

  });


})




var ajaxGeo = function(address) {

  $.ajax({

    url: "http://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&sensor=false&region=nz"

  }).done(function(res) {

    if (res.status === 'OK') {

      displayGeo(res.results);
    }

  })
}




var displayGeo = function(results) {

  var geos = [];

  sortGeo = _.bind(sortGeo, geos, results);

  sortGeo();

  _(geos).forEach(function(geo) {

    var location = geo.street + ', ' + geo.route + ', ' + geo.district + ', ' + geo.city + ', ' + geo.zipcode + '. (' + geo.lat + ', ' + geo.lng + ')';

    var value = geo.street + '|' + geo.route + '|' + geo.district + '|' + geo.city + '|' + geo.zipcode + '|' + geo.lat + '|' + geo.lng;

    $('#select-geo').append('<option value="' + value + '" >' + location + '</option>');

  })


}






var sortGeo = function(results) {

  var geos = this;

  _(results).forEach(function(result) {

    var geo = {};

    var components = result.address_components;

    geo.lat = result.geometry.location.lat;

    geo.lng = result.geometry.location.lng;

    _(components).forEach(function(component) {

      var types = component.types;

      if (_.contains(types, 'street_number')) {

        geo.street = component.long_name;

      } else if (_.contains(types, 'route')) {

        geo.route = component.short_name;

      } else if (_.contains(types, 'sublocality')) {

        geo.district = component.long_name;

      } else if (_.contains(types, 'locality')) {

        geo.city = component.long_name;

      } else if (_.contains(types, 'postal_code')) {

        geo.zipcode = component.long_name;

      } else {}
    })

    geos.push(geo);
  })

}








//
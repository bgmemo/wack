var jwt = require('jwt-simple');
var users = require('../app/actions/users');
var env = process.env.NODE_ENV || 'development';

var log4js = require('log4js');
var logger = log4js.getLogger();
log4js.replaceConsole();



exports.auth = function(req, res, next) {
  if (req.originalUrl.substring(0, 13) === '/api/v1/users') {
    next();
  } else {
    authorisation(req, res, next);
  }
}




var authorisation = function(req, res, next) {
  try {
    req.header('Token') ? validateToken(req, res, next) : noToken(req, res, next);
  } catch (err) {
    next(err);
  }
}




var validateToken = function(req, res, next) {
  var token = req.header('Token');
  var decoded = jwt.decode(token.replace(/['"]+/g, ''), req.locals.secret);

  if (Date.now() >= decoded.exp) {
    res.status(400).end();
  }

  users.attachReq(decoded.user, function(err, user) {
    if (err) next(err);
    req.user = user;
    logger.info(user);
    next();
  })
}




var noToken = function(req, res, next) {
  //If token exists, just ignore the hand shaking; if not, response 401
  if (req.header('access-control-request-method')) {
    next();
  } else {
    if (env === 'development') { //To test jwt, need to switch to test mode
      next();
      // res.status(401).send('You are so naughty!');
    } else {
      // res.status(401).send('You are so naughty!');
      next();
    }
  }
}




//
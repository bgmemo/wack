module.exports = function(app, hbs) {

  hbs.localsAsTemplateData(app);
  hbs.registerPartials(__dirname + '/../app/views/partials');



  var blocks = {};

  hbs.registerHelper('extend', function(name, context) {
    var block = blocks[name];
    if (!block) {
      block = blocks[name] = [];
    }
    block.push(context.fn(this));
  });




  hbs.registerHelper('block', function(name) {
    var val = (blocks[name] || []).join('\n');
    blocks[name] = [];
    return val;
  });




  hbs.registerHelper('header', function(context, options) {

    switch (this.route) {

      case 'main.home':
      case 'admin.login':
      case 'admin.register':
        break;

      default:
        return options.fn(context);
        break;
    }

  });

}
'use strict';
/**
 *
 */

var log4js = require('log4js');
var logger = log4js.getLogger();
log4js.replaceConsole();



process.on('uncaughtException', function(err) {
  logger.error('Uncaught Exception ' + err);
  process.exit(1);
});


/**
 * 
 */
var handler = function(err, req, res, next) {
  if (err) {
    logger.error(err);

    //html
    res.status(500).send('500 Error. Special case bro!');
    //json

  }


  // res.json({
  //   error: err ? err.message : "Internal Server Error"
  // });

  // var message = err ? err.message : "Internal Server Error";
  // res.json({
  //   error: {
  //     message: message
  //   }
  // })
  // logger.error({
  //   message: err.message,
  //   stack: err.stack
  // })
};



module.exports = handler;
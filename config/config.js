/**
 *
 * This is the configures of express and mongoDB.
 *
 * To-dos: Config password using arguments !important.
 *
 */


exports.secret = function(env) {
  var secret;

  switch (env) {
    case 'development':
      secret = process.env.SECRET_DEV;
      break;

    case 'test':
      secret = process.env.SECRET_TEST;
      break;

    case 'production':
    default:
      secret = process.env.SECRET_PROD;
      break;
  }

  return secret;
}


exports.mongo_url = function(env) {
  var mongo = {};

  switch (env) {
    case 'development':
      mongo.url = process.env.MONGO_DEV;
      break;

    case 'test':
      mongo.url = process.env.MONGO_TEST;
      break;

    case 'production':
    default:
      mongo.url = process.env.MONGO_PROD;
      break;
  }

  return mongo.url;
}



exports.session_url = function(env) {
  var mongo = {};

  switch (env) {
    case 'development':
      mongo.session_url = process.env.MONGO_DEV;
      break;

    case 'test':
      mongo.session_url = process.env.MONGO_TEST;
      break;

    case 'production':
    default:
      mongo.session_url = process.env.MONGO_PROD;
      break;
  }

  return mongo.session_url;
}
'use strict';
var os = require('os');
var env = process.env.NODE_ENV || 'development';

if (env === 'production' && os.platform() !== 'darwin') {
  require('newrelic');
}

require('dotenv').load();

  var express    = require('express'),
  compression    = require('compression'),
  methodOverride = require('method-override'),
  cookieParser   = require('cookie-parser'),
  bodyParser     = require('body-parser'),
  multer         = require('multer'),
  log4js         = require('log4js'),
  morgan         = require('morgan'),
  routeCache     = require('route-cache'),
  hbs            = require('hbs'),
  mongoose       = require('mongoose'),
  path           = require('path'),
  config         = require(__dirname + '/config/config'),
  hbsConfig      = require(__dirname + '/config/setting-hbs'),
  routes         = require(__dirname + '/app/routes'),
  errorHandler   = require(__dirname + '/config/errorhandler'),
  app            = express(),
  logger         = log4js.getLogger();

log4js.replaceConsole();

if (env === 'production') {
  app.use(morgan('combined', {
    skip: function(req, res) {
      return res.statusCode < 400
    }
  }));
}


mongoose.connect(config.mongo_url(env));
var db = mongoose.connection;
logger.info('** NODE_ENV: ' + app.get('env'));
db.on('error', console.error.bind(console, 'Connection error.'));
db.once('open', function() {
  logger.info('db opened! path: ' + __dirname);
});


hbsConfig(app, hbs);

app.set('views', __dirname + '/app/views');
app.set('view engine', 'html');
app.engine('html', hbs.__express);
app.set('view options', {
  layout: '/layouts/layout'
});


app.set("jsonp callback", true);
app.set("jsonp callback name", "callback");

if (env === 'production') {
  app.use(compression());
}


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
  extended: false
}))

// parse application/json
app.use(bodyParser.json());
app.use(cookieParser());
app.use(methodOverride(function(req, res) {
  if (req.body && typeof req.body === 'object' && '_method' in req.body) {
    var method = req.body._method;
    delete req.body._method;
    return method;
  }
}));

app.locals.secret = config.secret(env);

routes(express, app, hbs);

app.use(express.static(path.join(__dirname, '/public')));
app.use('/public/js', express.static(path.join(__dirname, '/public/js')));
app.use('/public/css', express.static(path.join(__dirname, '/public/css')));
app.enable('view cache');


if (env !== 'development') {
  app.use(routeCache.cacheSeconds(20));
}

app.use(errorHandler);

app.disable('x-powered-by');
app.listen('7777');

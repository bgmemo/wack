# Server of JEDI
This is the server end of [Jedi](https://bitbucket.org/bgmemo/jedi) project which is written in Express.js

## The following tools are adopted:

* Node.js

* Express.js

* Mongoose.js (MongoDB ODM)

* Lodash.js (Underscore)

* Q.js (Promises)
